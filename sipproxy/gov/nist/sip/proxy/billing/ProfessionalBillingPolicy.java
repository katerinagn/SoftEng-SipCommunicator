package gov.nist.sip.proxy.billing;


public class ProfessionalBillingPolicy implements BillingPolicy {

	double defaultCost = 5.0;
	double professionalCost = defaultCost / 1.5;
	
	@Override
	public double calculateCost(long duration) {
		return duration*professionalCost;
	}

}
